#include <ESP8266WiFi.h>
#include "Adafruit_MQTT.h"
#include "Adafruit_MQTT_Client.h"

/************************* WiFi Access Point *********************************/

#define WLAN_SSID       "WIFI_SSID"
#define WLAN_PASS       "WIFI_PASSWD"

/************************* Gbridge Setup *********************************/

#define AIO_SERVER      "192.168.1.158"       // mqtt server
#define AIO_SERVERPORT  1883                  // use 1883 or 8883 for SSL
#define AIO_USERNAME    "pi"                  // mqtt username
#define AIO_KEY         "raspberry"           // mqtt passwd

#define LED 2
#define BTN 4

char outputState = 0;
char lastState = 0;


// Create an ESP8266 WiFiClient class to connect to the MQTT server.
WiFiClient client;


// Setup the MQTT client class by passing in the WiFi client and MQTT server and login details.
Adafruit_MQTT_Client mqtt(&client, AIO_SERVER, AIO_SERVERPORT, AIO_USERNAME, AIO_KEY);


/****************************** Feeds ***************************************/
Adafruit_MQTT_Publish onoffset = Adafruit_MQTT_Publish(&mqtt, "home/room/desk/onoff"); 
Adafruit_MQTT_Subscribe onoffbutton = Adafruit_MQTT_Subscribe(&mqtt, "home/room/desk/onoff");

/*************************** Sketch Code ************************************/

void MQTT_connect();

void setup() {
  Serial.begin(115200);
  delay(10);

  pinMode(LED, OUTPUT);     // Initialize the LED pin as an output
  digitalWrite(LED, LOW);

  pinMode(LED_BUILTIN, OUTPUT);
  digitalWrite(LED_BUILTIN, LOW);   //Lights when not connected to MQTT

  pinMode(BTN, INPUT_PULLUP);     //BTN for manual light switch
  attachInterrupt ( digitalPinToInterrupt(BTN), toggle, RISING);

  attachInterrupt(digitalPinToInterrupt(0), handleInterrupt, FALLING);
  // Connect to WiFi access point.
  Serial.println();
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(WLAN_SSID);

  WiFi.begin(WLAN_SSID, WLAN_PASS);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println();

  Serial.println("WiFi connected");
  Serial.println("IP address: "); Serial.println(WiFi.localIP());

  // Setup MQTT subscription for onoff feed.
  mqtt.subscribe(&onoffbutton);
}

uint32_t x = 0;

void loop() {
  // Ensure the connection to the MQTT server is alive (this will make the first
  // connection and automatically reconnect when disconnected).  See the MQTT_connect
  // function definition further below.
  MQTT_connect();
  // this is our 'wait for incoming subscription packets' busy subloop
  // try to spend your time here

  Adafruit_MQTT_Subscribe *subscription;
  while ((subscription = mqtt.readSubscription(3000))) {
    if (subscription == &onoffbutton) {
      Serial.print(F("Got: "));
      Serial.println((char *)onoffbutton.lastread);
      if (strcmp((char *)onoffbutton.lastread, "1") == 0) {
        digitalWrite(LED, HIGH);
        outputState = 1;
      }
      if (strcmp((char *)onoffbutton.lastread, "0") == 0) {
        digitalWrite(LED, LOW);
        outputState = 0;
      }
    }

  }

  // Now we can publish stuff!
  if (outputState != lastState) {
    lastState = outputState;
    Serial.print(F("\nSending state val "));
    Serial.print(outputState, BIN);
    Serial.print("...");
    if (! onoffset.publish(outputState)) {
      Serial.println(F("Failed"));
    } else {
      Serial.println(F("OK!"));
    }
  }
}

void handleInterrupt() {    //works when button pressed
  static unsigned long last_interrupt_time = 0;
  unsigned long interrupt_time = millis();
  if (interrupt_time - last_interrupt_time > 200)
  {
    if (outputState == 0) {
      outputState = 1;
      digitalWrite(LED, LOW);
    }
    else {
      outputState = 0;
      digitalWrite(LED, HIGH);
    }
  }
  last_interrupt_time = interrupt_time;
  delay(100);
}


// Function to connect and reconnect as necessary to the MQTT server.
// Should be called in the loop function and it will take care if connecting.
void MQTT_connect() {
  int8_t ret;

  // Stop if already connected.
  if (mqtt.connected()) {
    return;
  }

  Serial.print("Connecting to MQTT... ");

  uint8_t retries = 3;
  while ((ret = mqtt.connect()) != 0) { // connect will return 0 for connected
    Serial.println(mqtt.connectErrorString(ret));
    Serial.println("Retrying MQTT connection in 5 seconds...");
    mqtt.disconnect();
    delay(5000);  // wait 5 seconds
    retries--;
    if (retries == 0) {
      // basically die and wait for WDT to reset me
      while (1);
    }
  }
  Serial.println("MQTT Connected!");
  digitalWrite(LED_BUILTIN, HIGH);
}


void toggle() {
  digitalWrite(LED, !outputState);
  outputState = !outputState;
}
